package nl.utwente.di.bikeDealer.resources;

import nl.utwente.di.bikeDealer.dao.BikeDao;
import nl.utwente.di.bikeDealer.model.Bike;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Path("/bikes")
public class BikesResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public List<Bike> getAllBikes() {
        MultivaluedMap<String,String> conditionsMap = uriInfo.getQueryParameters();
        return BikeDao.instance.returnBikes(conditionsMap);
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Bike newBike(Bike bike) throws IOException {
        System.out.println(bike.toString());
        BikeDao.instance.addBike(bike);
        return bike;
    }





    @Path("{bikes}")
    public BikeResource getBike(@PathParam("bikes") String id) {
        return new BikeResource(uriInfo, request, id);
    }


}
