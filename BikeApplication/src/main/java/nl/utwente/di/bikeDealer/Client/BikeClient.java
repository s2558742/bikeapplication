package nl.utwente.di.bikeDealer.Client;

import nl.utwente.di.bikeDealer.model.Bike;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BikeClient {

    public BikeClient() {

    }

    public static Scanner sc = new Scanner(System.in);

    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(getBaseURI());
    String url_pattern = "rest";
    String path = "bikes";

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080/bikeDealer").build();
    }

    public static void displayMenu() {
        System.out.println("1 = Print all bikes");
        System.out.println("2 = Print with conditions bikes");
        System.out.println("3 = Add bike");
        System.out.println("4 = Delete bike");
        System.out.println("5 = Close");

    }

    public void select1() {
        System.out.println("> all bikes (json): "
                + target.path(url_pattern).path(path).request()
                .accept(MediaType.APPLICATION_JSON).get(String.class));
    }

    public void select2() {
        System.out.println("Do you want to filter for color (0) or gender (1) or both (3)?");
        String in = sc.nextLine();

        if (Integer.parseInt(in) == 0) {
            System.out.println("Enter your condition for colour: ");
            in = sc.nextLine();
            System.out.println(target.path(url_pattern).path(path)
                    .queryParam("colour", in));
            System.out.println(target.path(url_pattern).path(path)
                    .queryParam("colour", in).request()
                    .accept(MediaType.APPLICATION_JSON).get(String.class));
        } else if (Integer.parseInt(in) == 1) {
            System.out.println("Enter your condition for gender: ");
            in = sc.nextLine();
            System.out.println(target.path(url_pattern).path(path)
                    .queryParam("colour", in).request()
                    .accept(MediaType.APPLICATION_JSON).get(String.class));
        } else if (Integer.parseInt(in) == 3) {
            List<String> filters = new ArrayList<>();
            System.out.println("Enter your condition for colour: ");
            in = sc.nextLine();
            filters.add(in);
            System.out.println("Enter your condition for gender: ");
            in = sc.nextLine();
            filters.add(in);
            System.out.println(target.path(url_pattern).path(path)
                    .queryParam("colour", filters.get(0)).queryParam("gender", filters.get(1)).request()
                    .accept(MediaType.APPLICATION_JSON).get(String.class));
        }
    }

    public void select3(){
        System.out.print("Enter your id : ");
        String id = sc.nextLine();
        System.out.print("Enter your owner : " );
        String owner = sc.nextLine();
        System.out.print("Enter your colour : " );
        String colour = sc.nextLine();
        System.out.print("Enter your gender : " );
        String gender = sc.nextLine();

        Bike bike = new Bike(id, owner, colour, gender);
        Response response = target.path(url_pattern).path(path)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(bike, MediaType.APPLICATION_JSON));
        // Return code for created resource
        System.out.println("> create bike - result status: "
                + response.getStatus());
    }


    public void select4(){
        System.out.print("What is the ID of the bike you want to delete");
        String id = sc.nextLine();
        target.path(url_pattern).path(path + "/" + id).request().delete();
        System.out.println("> get all bikes after delete bike 1: "
                + target.path(url_pattern).path(path).request()
                .accept(MediaType.APPLICATION_JSON).get(String.class));

    }


    public static void main(String[] args) {

        BikeClient bc = new BikeClient();

        while(true) {
            displayMenu();
            String input = sc.nextLine();
            switch (Integer.parseInt(input)) {
                case 1:
                    bc.select1();
                    break;
                case 2:
                    bc.select2();
                    break;
                case 3:
                    bc.select3();
                    break;
                case 4:
                    bc.select4();
                    break;
                case 5:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid input");
                    break;
            }
        }

    }

}
