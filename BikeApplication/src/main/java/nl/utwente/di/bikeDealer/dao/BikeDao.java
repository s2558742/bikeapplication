package nl.utwente.di.bikeDealer.dao;

import nl.utwente.di.bikeDealer.model.Bike;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BikeDao {
    instance;

    private Map<String, Bike> bikeMap = new HashMap<>();
    private Map<String, Bike> orderedBikes = new HashMap<>();


    private BikeDao() {
        Bike bike1 = new Bike("6", "Mustafa", "red", "male");
        bikeMap.put(bike1.getId(), bike1);

        Bike bike2 = new Bike("7", "Faisal", "Blue", "male");
        bikeMap.put(bike2.getId(), bike2);
    }

    public List<Bike> returnBikes(MultivaluedMap<String,String> conditionsMap) {
        if (conditionsMap.size() == 0){
            List<Bike> bikes = new ArrayList<>();
            bikes.addAll(bikeMap.values());
            return bikes;
        } else {
            List<Bike> bikes = new ArrayList<>();
            List<String> conditions = new ArrayList<>();
            for (var x : conditionsMap.keySet()){
                conditions.add(x);
            }
            if (conditions.contains("colour") && conditions.contains("gender")){
                for (Bike x : bikeMap.values()){
                    if(x.getColour().equals(conditionsMap.get("colour").get(0)) && x.getGender().equals(conditionsMap.get("gender").get(0))){
                        bikes.add(x);
                    }
                }
            } else if (conditions.contains("colour")){
                for (Bike x : bikeMap.values()){
                    if(x.getColour().equals(conditionsMap.get("colour").get(0))){
                        bikes.add(x);
                    }
                }

            } else if (conditions.contains("gender")){
                for (Bike x : bikeMap.values()){
                    if(x.getGender().equals(conditionsMap.get("gender").get(0))){
                        bikes.add(x);
                    }
                }
            }
            return bikes;
        }
    }

    public String addBikeToOrder(String id) {
        if (orderedBikes.containsKey(id)) {
            return "Unsuccesful, item already in cart.";
        } else {
            orderedBikes.put(id, bikeMap.get(id));
            return "Succesfully added";
        }

    }

    public Bike getSpecificBike(String id) {
        return bikeMap.get(id);
    }

    public void deleteBike(String id){
        bikeMap.remove(id);
    }

    public void addBike(Bike bike){
        bikeMap.put(bike.getId(),bike);
    }

}
