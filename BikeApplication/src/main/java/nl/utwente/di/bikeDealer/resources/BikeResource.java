package nl.utwente.di.bikeDealer.resources;

import nl.utwente.di.bikeDealer.dao.BikeDao;
import nl.utwente.di.bikeDealer.model.Bike;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

public class BikeResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    String id;

    public BikeResource(UriInfo uriInfo, Request request, String id) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.id = id;
    }


    @GET
    @Produces({MediaType.APPLICATION_JSON})

    public Bike getBike() {
        Bike bike = BikeDao.instance.getSpecificBike(id);
        return bike;
    }

    @DELETE
    public void deleteBike(){
        BikeDao.instance.deleteBike(id);
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Bike updateBike(Bike bike){
        if (this.id.equals(bike.getId())){
            BikeDao.instance.addBike(bike);
        } else {
            BikeDao.instance.deleteBike(id);
            BikeDao.instance.addBike(bike);
        }
        return bike;
    }

    @POST
    @Path("/order")
    @Produces({MediaType.TEXT_PLAIN})
    public String orderBike() {
        return BikeDao.instance.addBikeToOrder(id);
    }




}
